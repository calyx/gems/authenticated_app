require_relative "lib/authenticated_app/version"

Gem::Specification.new do |spec|
  spec.name        = "authenticated_app"
  spec.version     = AuthenticatedApp::VERSION
  spec.authors     = ["elijah"]
  spec.homepage    = "https://0xacab.org/calyx/gems/authenticated_app"
  spec.summary     = "A mountable Rails Engine providing basic authentication and session management."
  spec.license     = "AGPL"

  spec.files = Dir["{app,config,db,lib}/**/*", "LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails"
  spec.add_dependency "haml"
  spec.add_development_dependency "sqlite3"

  # password hashing
  spec.add_dependency "rbnacl", "~> 7.1.1"
  spec.add_dependency "zxcvbn-ruby", "~> 0.1"
  spec.add_dependency "bcrypt", "~> 3.1"
  spec.add_dependency "unix-crypt", "~> 1.3.0"
end
