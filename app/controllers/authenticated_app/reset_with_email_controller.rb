#
# Reset password using alternate email and a token.
#
# step 1. GET enter_email
# step 2. PUT check email -- confirm email and username combination
# step 3. GET enter password
# step 4. PUT update password -- confirm user and token combination
#
class AuthenticatedApp::ResetWithEmailController < AuthenticatedApp::ResetController
  before_action :setup

  # get /reset_with_email/enter_email
  def enter_email
  end

  # put /reset_with_email/check_email
  def check_email
    @user = if AuthenticatedApp.email_only
      get_user_from_email
    else
      get_user_from_username_and_email
    end
    if !@user || !send_token_email(user: @user, email: params[:recovery_email])
      render :enter_email
    else
      session[:recovery_user]  = @user.login
      session[:recovery_email] = params[:recovery_email]
      redirect_to reset_with_email_enter_password_url
    end
  end

  # get /reset-my-password/:recovery_user/:token
  def enter_password
    @user = get_user_from_username
    if @user && params[:token]
      @token_valid = @user.recovery_token_valid?(params[:token])
      if !@token_valid
        @errors[:token] = t('errors.format', attribute: t(:token), message: t('errors.messages.invalid'))
      end
    end
  end

  # put /reset_with_email/update_password
  def update_password
    @user = get_user_from_username
    if !@user
      render :enter_email
    elsif !token_ok?(@user)
      render :enter_password
    elsif !password_ok?(@user)
      render :enter_password
    elsif !save_new_password(@user)
      render :enter_password
    else
      login_as @user
    end
  end

  protected

  def validate_user_param
    if !params[:recovery_user].present?
      @errors[:recovery_user] = t('errors.format', attribute: t(:username), message: t('errors.messages.blank'))
    end
    return @errors.empty?
  end

  def validate_email_param
    if !params[:recovery_email].present?
      @errors[:recovery_email] = t('errors.format', attribute: t(:recovery_email), message: t('errors.messages.blank'))
    elsif ValidatesEmailFormatOf::validate_email_format(params[:recovery_email]) != nil
      @errors[:recovery_email] = t('errors.format', attribute: t(:recovery_email), message: t('errors.messages.invalid'))
    end
    return @errors.empty?
  end

  def get_user_from_email
    user = nil
    if validate_email_param
      user = User.find_by_email(params[:recovery_email])
      if user.nil?
        @errors[:recovery_email] = true
        flash.now[:danger] << t(:username_or_recovery_email_is_incorrect)
      end
    end
    user
  end

  def get_user_from_username_and_email
    user = nil
    if validate_user_param && validate_email_param
      user = User.find_by_login(params[:recovery_user])
      if user.nil? || !user.recovery_email_valid?(params[:recovery_email])
        @errors[:recovery_user] = true
        @errors[:recovery_email] = true
        flash.now[:danger] << t(:username_or_recovery_email_is_incorrect)
        return nil
      end
    end
    user
  end

  def get_user_from_username
    user = nil
    if validate_user_param
      user = User.find_by_login(params[:recovery_user])
      if user.nil?
        @errors[:recovery_user] = t('errors.format', attribute: t(:username), message: t('errors.messages.invalid'))
      end
    end
    user
  end

  def token_ok?(user)
    unless user.recovery_token_valid?(params[:token])
      @errors[:token] = t('errors.format', attribute: t(:token), message: t('errors.messages.invalid'))
      return false
    else
      @token_valid = true
      return true
    end
  end

  def save_new_password(user)
    user.hard_reset_password!(new_password: params[:new_password])
    user.update_columns(recovery_token: nil, recovery_token_expires_at: nil)
    return true
  rescue StandardError => exc
    flash.now[:danger] = t(:changes_not_saved)
    return false
  end

  def send_token_email(user:, email:)
    if !user.recovery_email_valid?(email)
      @errors[:recovery_email] = user.errors.full_message(:recovery_email, user.errors.generate_message(:recovery_email))
      return false
    elsif helpers.send_password_reset_email(user, email)
      flash[:success] = t(:check_email_for_token)
      return true
    else
      @errors[:recovery_email] = "Could not create recovery token"
      return false
    end
  end

  def setup
    params[:recovery_user]  ||= session[:recovery_user]
    params[:recovery_email] ||= session[:recovery_email]
    if params[:recovery_user]
      params[:recovery_user] = helpers.url_decode_email(params[:recovery_user].downcase.strip)
    end
    if params[:recovery_email]
      params[:recovery_email] = params[:recovery_email].downcase.strip
    end
  end
end
