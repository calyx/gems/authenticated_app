class AuthenticatedApp::PasswordsController < ApplicationController
  RANDOM_LENGTH = 12
  RANDOM_SPLIT  = 4

  def show
    @errors = {}
    case params[:view]
    when nil then
      render :show
    when 'recovery' then
      render :show_recovery
    when 'services' then
      render :show_services
    end
  end

  def update
    case params[:update_action]
    when 'update_password' then
      update_password
    when 'update_recovery' then
      update_recovery
    when 'update_recovery_email' then
      update_recovery_email
    when 'update_service_password' then
      update_service_password
    else
      redirect_to passwords_url
    end
  end

  protected

  def update_password
    @errors = {}

    if current_user.primary_secret && !current_user.authenticated?(params[:current_password])
      @errors[:current_password] = t(:current_password_is_incorrect)
    end

    if !current_user.password_strong_enough?(params[:new_password])
      @errors[:new_password] = current_user.errors.full_messages_for(:password)
    end

    if params[:new_password] != params[:password_confirmation]
      @errors[:password_confirmation] = t(:password_confirmation_error)
    end

    if @errors.empty?
      begin
        current_user.update_main_password!(
          password: params[:current_password],
          new_password: params[:new_password]
        )
      rescue StandardError => exc
        if current_user.errors.any?
          @errors = current_user.errors
        else
          @errors = exc
        end
      end
    end

    if @errors.any?
      flash.now[:danger] = t(:changes_not_saved)
      render :show
    else
      flash[:success] = t(:new_password_saved)
      redirect_to passwords_url
    end
  end

  def update_recovery
    if !current_user.authenticated?(params[:current_password])
      flash.now[:danger] = t(:changes_not_saved)
      @rc_errors = {:current_password => t(:password_is_incorrect)}
    else
      @recovery_code = current_user.create_random_recovery!(password: params[:current_password])
    end
  rescue StandardError => exc
    logger.error("ERROR: could not update recovery code for user %s: %s" % [current_user.username, exc.to_s])
    flash.now[:danger] = t(:changes_not_saved)
    @recovery_code = nil
  ensure
    render :show_recovery
  end

  #
  # save a new value for the recovery email. do not require a password
  # when clearing an old value.
  #
  def update_recovery_email
    @errors = {}
    new_email = params[:clear_value] ? "" : params.require(:user)[:recovery_email]
    if params[:clear_value].nil? && !current_user.authenticated?(params[:current_password])
      flash.now[:danger] = t(:changes_not_saved)
      @errors[:current_password] = t(:password_is_incorrect)
      render :show_recovery
    else
      current_user.recovery_email = new_email
      current_user.save!
      if current_user.recovery_email.present?
        flash[:success] = t(:changes_saved) + " (%s)" % new_email
      else
        flash[:success] = t(:changes_saved)
      end
      redirect_to passwords_url(:view => 'recovery')
    end
  rescue StandardError => exc
    flash.now[:danger] = t(:changes_not_saved)
    render :show_recovery
  end

  def update_service_password
    if params[:randomize]
      randomize_service
    elsif params[:clear]
      clear_service
    elsif params[:save]
      save_service
    else
      redirect_to passwords_url(view: 'services')
    end
  end

  #
  # create a new random service password
  #
  def randomize_service
    random = RandomCode.create(RANDOM_LENGTH, RANDOM_SPLIT)
    if params[:field] == 'chat_password'
      current_user.update_password!(password: random, service: :chat)
      flash[:success] = [
        t(:new_chat_password) % bold(random),
        t(:changes_saved)
      ]
    elsif params[:field] == 'vpn_password'
      current_user.update_password!(password: random, service: :vpn)
      flash[:success] = [
        t(:new_vpn_password) % bold(random),
        t(:changes_saved)
      ]
    end
    redirect_to passwords_url(view: 'services')
  rescue StandardError => exc
    flash.now[:danger] = exc
    render :show_services
  end

  #
  # clear the service password
  #
  def clear_service
    if params[:field] == 'chat_password'
      current_user.update_password!(password: nil, service: :chat)
      flash[:success] = t(:password_removed)
    elsif params[:field] == 'vpn_password'
      current_user.update_password!(password: nil, service: :vpn)
      flash[:success] = t(:password_removed)
    end
    redirect_to passwords_url(view: 'services')
  rescue StandardError => exc
    flash.now[:danger] = exc
    render :show_services
  end

  #
  # clear the service password
  #
  def save_service
    @errors = {}
    if params[:field] == 'chat_password'
      if not_strong(params[:chat_password])
        current_user.errors.add(:password, t(:not_strong_enough))
        @errors['chat_password'] = true
      else
        current_user.update_password!(password: params[:chat_password], service: :chat)
        flash[:success] = t(:changes_saved)
      end
    elsif params[:field] == 'vpn_password'
      if not_strong(params[:vpn_password])
        current_user.errors.add(:password, t(:not_strong_enough))
        @errors['vpn_password'] = true
      else
        current_user.update_password!(password: params[:vpn_password], service: :vpn)
        flash[:success] = t(:changes_saved)
      end
    end

    if @errors.any?
      flash.now[:danger] = current_user.errors
      render :show_services
    else
      redirect_to passwords_url(view: 'services')
    end
  rescue StandardError => exc
    flash.now[:danger] = exc.to_s
    render :show_services
  end

  #
  # danger! never pass an insecure string to this method.
  #
  def bold(str)
    "[b]#{str}[/b]"
  end

  def not_strong(str)
    str.nil? || str.length < 8
  end
end
