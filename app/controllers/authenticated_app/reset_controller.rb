#
# Abstract super class of reset_with_code_controller and reset_with_email_controller
#

class AuthenticatedApp::ResetController < ApplicationController
  before_action :setup_error
  skip_before_action :require_authentication

  protected

  def login_as(user)
    may_authenticate!(user)
    reset_session
    flash[:success] = t(:changes_saved)
    session[:user_id]   = user.id
    session[:domain_id] = user.domain_object.id if user.respond_to?(:domain_object)
    redirect_to app_home_url
  end

  def password_ok?(user)
    if !user.password_strong_enough?(params[:new_password])
      @show_password_tips = true
      @errors[:new_password] = user.errors.full_messages_for(:password)
    end
    if params[:new_password] != params[:password_confirmation]
      @errors[:password_confirmation] = t(:password_confirmation_error)
    end
    if @errors.any?
      flash.now[:danger] << t(:changes_not_saved)
      return false
    else
      return true
    end
  end

  def setup_error
    flash.now[:danger] = []
    @errors = {}
  end

end

