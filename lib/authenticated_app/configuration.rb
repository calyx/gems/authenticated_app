module AuthenticatedApp
  class Configuration
    attr_accessor :home_url       # home when authenticated
    attr_accessor :root_url       # home when not authenticated
    attr_accessor :email_only     # if true, we ignore username and just use email
    attr_accessor :from_address   # From header for outgoing emails
    attr_accessor :default_domain # default domain to use for a user if domain is not explicitly set

    def initialize
      @home_url = :home_url
      @root_url = :root_url
      @email_only = false
      @from_address = nil
      @default_domain = "example.net"
    end
  end
end