module AuthenticatedApp
  class Engine < ::Rails::Engine
    isolate_namespace AuthenticatedApp
    config.autoload_paths << File.expand_path('../..', __FILE__) # absolute path to `authenticated_app/lib/`
  end
end
