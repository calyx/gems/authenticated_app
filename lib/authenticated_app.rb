require "authenticated_app/version"
require "authenticated_app/engine"
require "authenticated_app/configuration"

module AuthenticatedApp
  def self.configure
    yield config
  end

  def self.config
    @config ||= Configuration.new
  end
end
