require_relative '../../test_helper'

class AuthenticatedApp::SessionsControllerTest < ActionController::TestCase
  def setup
    @routes = AuthenticatedApp::Engine.routes
  end

  def test_new
    get :new
    assert_response :success
  end

  def test_fail_create
    post :create
    assert_response :success
    assert flash[:danger].present?
  end

  def test_create
    session[:msg] = "session should clear on login"
    user = create_user
    post :create, params: {login: user.username, password: user.password}
    assert flash[:danger].blank?, flash[:danger]
    assert session[:msg].blank?, session[:msg]
    assert_redirected_to @controller.send(:app_home_url)
  end

  def test_destroy
    session[:user_id] = create_user.id
    delete :destroy
    assert session[:user_id].blank?
    assert_redirected_to @controller.send(:app_root_url)
  end

  # TODO

  #def test_login_should_restore_inactive_account
  #  user = users(:green)
  #  user.close(nil) # closing without a user will mark it as closed by inactivity
  #  assert !user.is_active?
  #  post :create, username: "green", password: "password"
  #  assert flash[:danger].blank?, flash[:danger]
  #  assert_redirected_to user_path
  #  assert user.reload.is_active?
  #end

  #def test_login_should_not_restore_admin_closed_account
  #  user = users(:green)
  #  user.close(users(:red))
  #  assert !user.is_active?
  #  post :create, username: "green", password: "password"
  #  assert !flash[:danger].blank?
  #  assert !user.reload.is_active?
  #end

end
