Rails.application.routes.draw do
  root 'root#show'

  mount AuthenticatedApp::Engine => "/authenticated_app"
end
