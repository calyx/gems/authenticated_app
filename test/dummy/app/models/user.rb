class User < ActiveRecord::Base
  include UserCore
  include UserUsername
  include UserAuthentication
  include UserRecoveryEmail
end
